# Gitlab 

Gitlab is a lot more open source than Github seems to be which is bought out by 
Microsoft and so runs very much most of the world in that sense, right? It is one 
of  the largest codebase  platforms that people use  for simple version control 
for their programs but similar in the way that google docs is hosted by Google, the
very code that we use is technically all under Microsoft.

Also since you haven't signed anything yet for Windtelligent.ai this can still 
technically apply to you: https://about.gitlab.com/solutions/startups/

Similar to github gists, gitlab uses snippets. https://gitlab.com/-/snippets/new

GitDock is a GUI for your git projects: https://www.youtube.com/watch?v=WkVS38wo4_w

This is  my  gitlab profile: https://gitlab.com/shwetha729

..made with BBEdit 