# JSLint

According to the [instructions](https://www.jslint.com/help.html) doc, JSLint is a code quality tool for JavaScript. 

The Principle of the Good Parts is

> If a feature is sometimes useful and sometimes dangerous and if there is a better option then always use the better option.

It can be accessed [here](https://www.jslint.com). Really understanding what is being explained here is key to using JSLint properly when  debugging JavaScript. 

And it was written in [BBEdit](obsidian://open?vault=Coding%20Tips&file=Computers%2FMac%20OS%20X%2FBBEdit). 