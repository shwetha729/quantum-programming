# CSS

Ah the design and creative part of front end coding. This is what I started out doing when I was around 12 years old on neopets.com.  The world has gotten a lot better since then now and we have  many  options for web design and  front end programming. 

---


- One of  the best tools  to use for CSS is https://tailwindcss.com/
	- for more info on how to set it up: https://tailwindcss.com/docs/installation