# CLM 


CLM stands for Codeless  Language Module  done via BBEdit most commonly. It is the easiest way to  define things in actionable ways within your machine. And the possibiltiies are truly endless. 
Here is the [example](obsidian://open?vault=Coding%20Tips&file=Computers%2FMac%20OS%20X%2FBBEdit%2FExample%20CLM) for a simple CLM. 

CLMs take the form  of property list files (plists)
There are tons of information about CLMs on this [page](http://www.barebones.com/support/develop/clm.html).   Read very carefully ;) 


And, indeed, this is different than  [Language Server Protocols](http://www.barebones.com/support/bbedit/lsp-notes.html),  or  LSPs,  for short.