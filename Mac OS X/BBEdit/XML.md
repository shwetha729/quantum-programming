# XML 

Extensible Markup Language (**XML**) is a markup language and file format for storing, transmitting, and reconstructing arbitrary data. It defines a set of rules for encoding documents in a format that is both human-readable and machine-readable. 

Utilized often in [BBEdit](obsidian://open?vault=Coding%20Tips&file=Computers%2FMac%20OS%20X%2FBBEdit) for CLMs.  