# Example

This is an example as given in the CLM doc of a "My Language" file, a severe subset of the Ruby scripting language. 

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN"
	"http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">

<!--  The smallest CLM that will color keywords.
      Copyright (c) 2012 Bare Bones Software, Inc. -->
		
<dict>
	<!-- You must identify the plist as a CLM: -->
	<key>BBEditDocumentType</key>
	<string>CodelessLanguageModule</string>
	
	<!-- You must identify your language: -->
	<key>BBLMLanguageDisplayName</key>  <string>My Language</string>
	<key>BBLMLanguageCode</key>         <string>MyL!</string>
	
	<!-- Not required, but there’s not much point to
	     a language module if it doesn’t color syntax: -->
	<key>BBLMColorsSyntax</key>         <true/>
	
	<!-- Specify some keywords. This isn’t required, either,
             but it provides something to color: -->
	<key>BBLMKeywordList</key>
	<array>
		<string>class</string>
		<string>def</string>
		<string>if</string>
		<string>elsif</string>
		<string>else</string>
		<string>end</string>
		<string>do</string>
		<string>for</string>
		<string>return</string>
	</array>
	
	<!-- Specify a comment-out string: -->
	<key>BBLMCommentLineDefault</key>   <string>#</string>
	
	<!-- You must specify the character set for
             keywords and identifiers. Substitute your own:  -->
	<key>Language Features</key>
	<dict>
		<key>Identifier and Keyword Character Class</key>
		<string>A-Za-z0-9_\?!</string>
	</dict> 
</dict>
</plist>
```

**Things  to notice here:**

-   If you don’t want the module to do anything other than take up space in the language pop-up, just specify `BBEditDocumentType`, `BBLMLanguageDisplayName`, and `BBLMLanguageCode`. But this is the minimal _useful_ language module.
-   For syntax coloring, you must turn on `BBLMColorsSyntax`.
-   For something to color, you must provide a list of keywords in `BBLMKeywordList`.
-   BBEdit requires that you provide a method — line or block — for commenting-out lines with the Un/Comment Selection command. In this case, `BBLMCommentLineDefault` specifies the # line-comment token.
-   You must also specify how to identify keywords, in the `Language Features `dictionary, using either `Identifier` and `Keyword Characters `or `Identifier` and `Keyword Character`  `Class`.
-   You _don’t_ have to provide a `BBLMSuffixMap` or `BBLMFileNamesToMatch` list. Without them, your user will have to pick the language out of the pop-up.

For more info, go [here](http://www.barebones.com/support/develop/clm.html#Examples). 