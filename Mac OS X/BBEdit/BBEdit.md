# BBEdit

 *BBEdit : the Swiss Army **Chainsaw** of Programmers' Editor*



It simple but powerful text  editor with lots of [new](http://www.barebones.com/products/bbedit/bbedit14.html) [features](http://www.barebones.com/products/bbedit/features.html) with its [BBEdit 1](http://www.barebones.com/support/bbedit/notes-14.0.html)4 release. 


### Language Modules: 
Notice, that it doesn't have python,  html, java/javascript or more popular languages among the list..
- interestingly, there is a feature where if you are designing your own language,  you may be able to create am "Edit in BBEdit" command. 
	- in fact, it even seems to be [encouraged](http://www.barebones.com/support/develop/). 


- the [codeless language module reference](http://www.barebones.com/support/develop/clm.html#TypesOfCodelessLanguageModules) is made to make things extremely easy for you 
	- there is even a  section where you can pull  up docs for a language  within  a keyword. 
	- ![[Pasted image 20220908174202.png]]
	- In this context, “symbols” are any runs of text defined by the “[Identifier and Keyword](http://www.barebones.com/support/develop/clm.html#IdentifyingKeywordStrings)” character sets. Spelling-dictionary words will be offered only if the user has enabled the feature in the “Editing” preferences panel.



For any questions or issues you encounter, start a post on the [BBEdit google group](https://groups.google.com/g/bbedit). Or if something crashes, contact [tech support](https://www.barebones.com/contact/technical.html). 

P.S. [Yojimbo](http://www.barebones.com/products/yojimbo/tour-end.html) is a fantastic partner to work alongside BBEdit for quickly storing your files for viewing. The benefit of  this  is that you can use  BBEdit to simply write,  and you can use Yojimbo to view, store,  collect,  and take out pretty much anything your mind can think of. This is so fantastic because it is intuitively  how we organize information even in the real world as  well, part of the "stutter"  that happens with human/computer interaction  is when things are not intuitive. By providing this metaphorical interface,  Yojimbo is infinitely ahead of  the cloud game which, although may be fast and can store more data "online", is terrible for the environment and doesn't lead to  actual real-world  outputs.  