This work is licensed under a [Creative Commons Attribution-NonCommercial 2.5 License](https://creativecommons.org/licenses/by-nc/2.5/).  
  
This means that you are **free** to copy and reuse any of my information & drawings (noncommercially) as long as you tell people where they're from.


