# Welcome! 

You've become curious as any adventurer would and have  decided to embark on a grand journey: the journey to understand quantum - in all of its computation, application, and more. It's a journey I'm undergoing myself as I begin to write this and instead of creating an archive of  quantum papers, code, games, platforms, tools, research, or more I thought it'd be more impactful to create an obsidian knowledge base  as I continue to grow into the quantum journey along  with you all. 

This will be helpful to me as both review and discovery as well as hopefully to you  and others who have decided in various moments in life to  begin the  path to conquer  what lies at the end of the quantum quest. 



[Next up](obsidian://open?vault=Coding%20Tips&file=Computers%2FQuantum%20Realm%2FIntro%20to%20Quantum%20Technologies): Look over at THE intro to quantum tech to get up to speed. [--> ](obsidian://open?vault=Coding%20Tips&file=Computers%2FQuantum%20Realm%2FTechnologies%2FIntro%20to%20Quantum%20Technologies)

Welcome & Good Luck! ^-^  

---

##### When finished: the 3 current main applications 
As said by Konstantinos there are generally **three large pillars** of application to use QC for: 

1. [Optimizations](obsidian://open?vault=Coding%20Tips&file=Quantum%20Optimizations) 
2.[ Machine Learning ](obsidian://open?vault=Coding%20Tips&file=Quantum%20Machine%20Learning)
3. [Simulations](obsidian://open?vault=Coding%20Tips&file=Quantum%20Simulations) 

