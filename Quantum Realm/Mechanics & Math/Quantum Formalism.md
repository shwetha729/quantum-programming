# Formalism 



**Abstract:  **


Please  humor me if this  is a stretch but I thought this something worth exploring from a more  mathematical perspective and proof: As radio waves are currently the  longest emitting  electromagnetic  frequencies,  would  they  play a role in accounting for noise specifically in wind by formulating  a way in which radio  waves detecting noise via wind can communicate this rather naturally in a math formulation to quantum computing. There seems to be a lot of similarity between these  two  industries and rather  than approaching it from a photonic  or superconducting/circuit perspective,  I wanted  to take a moment  to  understand if this can be better formalized in an equation demonstrating the relationship between the earth’s wind, radio waves tested via  ham amateur radio, and factoring these into error mitigation in quantum computing. Although many  things may  account for  the noise that leaves  quantum  computers largely still unusable today, such as natural earth perturbations, I am still  interested in primarily exploring atmospheric  wind in a mathematical approach.

