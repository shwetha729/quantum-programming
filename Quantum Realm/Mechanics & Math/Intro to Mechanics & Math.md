# Intro/Summary of Quantum Mechanics
The formalisms of somewhat knowing what each part of this is called will be somewhat important for you to seem knowledgeable in what you're talking about but might not be as necessary in application. Regardless, it'll give you major street cred around the quantum realm. 

- Quantum  formalism  will help understand what kind of math it's based on 
	- primarily linear algebra & matrices 
- Funny enough, another Obsidian user has made a **much** more extensive guide called [The Quantum Well ](https://publish.obsidian.md/myquantumwell/Welcome+to+The+Quantum+Well!)which presents most of the math and physics you will ever need to know. 
- since my Obsidian focuses primarily on the programming and tech, the above foundational knowledge  will be useful for those  that are curious. 

