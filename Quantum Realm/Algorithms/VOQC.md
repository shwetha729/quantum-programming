# VOQC

VOQC, also pronounced as  *vox*, is short for verfied optimizer for quantum circuits. Since many  current quantum frameworks use qasm files, voqc was created as a way  to debug circuits from compile error upon construction, as is my understanding anyways. The official paper though can be found [here](https://arxiv.org/pdf/1912.02250.pdf). And it is pretty informative! In order to install fully though, you will also need  to install [opam](obsidian://open?vault=Coding%20Tips&file=Opam). 

---

**Example: Quantum teleportation:** 

The idea behing quantum teleportation is transmit a state psi from one party (Alice) to another (Bob) using a shared entangled state. A circuit for this is shown in a corresponding SQIR program. 

```
Definition bell: ucom base 3 :=  H 1; CNOT 1 2 
Definition alice: com base 3 := CNOT 0 1 ; H 0; measure 0; measure 1.
Definition bob : com base 3 := CNOT 1 2; Cz 0 1;  reset 0; reset 1.
Definition  teleport : com base 3 := bell; alice, bob
```

SQIR : a quantum language  pronounced *squire* which stands for small  quantum intermediate representation. They maintain a [github](https://github.com/inQWIRE/SQIR) with several examples of implementation. Upon implementation,  there  seems to be an extraction process as well which is further documented [here](https://github.com/inQWIRE/mlvoqc). It takes a highly mathematical approach and is compatibly based on COQ. 

More information of Verified Quantum Computing can also be found [here](http://www.cs.umd.edu/~rrand/vqc/index.html). 

---

##  An example of  optimization! 

- Reading this [tutorial](https://nbviewer.org/github/inQWIRE/pyvoqc/blob/main/tutorial.ipynb) is extremely helpful in understanding what the point of  voqc is. It makes  constructing circuits a lot easier than  having to understand all the gate logic and  needing to create them all  by hand. 