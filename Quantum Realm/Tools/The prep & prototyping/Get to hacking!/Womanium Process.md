# Womanium 2022

Here I will go through all research, steps, processes, tests, and methods used for the Womanium Hackathon. 

The main github repo: https://github.com/spendierk/Womanium_Hackathon_TKET_2022

*Primary Goals include:*
 - [Building](https://github.com/spendierk/Womanium_Hackathon_TKET_2022/blob/main/Building%20circuits%20with%20pytket.ipynb) circuits with ``pytket``
 - comparing againsts others with [Arline Benchmarks](obsidian://open?vault=Coding%20Tips&file=Arline%20Benchmarks)
 - Solving for the hydrogen storage problem  with LiCl  molecule as found [here](https://qiskit.org/documentation/nature/tutorials/03_ground_state_solvers.html). 

---


## A-Z: Arline Benchmarks: 
1. Firstly, it was discussed that the [arline benchmark](https://github.com/ArlineQ/arline_benchmarks)s will be a good place to begin to compare quantum processing speeds!  Begin by following that and installing  that on your machine. 

- the verified  optimizer for quantum circuits or [VOQC](https://github.com/inQWIRE/pyvoqc) is another  analysis  tool we will need. Install that as well. 