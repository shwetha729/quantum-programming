# Qiskit -Nature 


The main cloud application that uses superconducting qubits is qisket. Qisket Nature depends  on the main package. 

- [Thermodynamic observable calculations ](https://qiskit.org/documentation/nature/tutorials/06_calculating_thermodynamic_observables.html)with IBM quantum 

### Installation 

```
pip install qiskit[nature]
```

Further instructions can be read [here](https://qiskit.org/documentation/nature/getting_started.html).  Installed via xonsh. 

---
For hydrogen storage notebook --> use the ground state solver docs: 

### 1. Define a molecular system: 

Ask for the electronic  part of a hydrogen molecule: 
```
from qiskit import Aer
from qiskit_nature.drivers import UnitsType, Molecule
from qiskit_nature.drivers.second_quantization import (
    ElectronicStructureDriverType,
    ElectronicStructureMoleculeDriver,
)
from qiskit_nature.problems.second_quantization import ElectronicStructureProblem
from qiskit_nature.converters.second_quantization import QubitConverter
from qiskit_nature.mappers.second_quantization import JordanWignerMapper

molecule = Molecule(
    geometry=[["H", [0.0, 0.0, 0.0]], ["H", [0.0, 0.0, 0.735]]], charge=0, multiplicity=1
)
driver = ElectronicStructureMoleculeDriver(
    molecule, basis="sto3g", driver_type=ElectronicStructureDriverType.PYSCF
)

es_problem = ElectronicStructureProblem(driver)
qubit_converter = QubitConverter(JordanWignerMapper())
```

### 2. Define solver: 
A solver aka the algorithm that the ground state is computed with. In chemistry the gound state is found with **variational quantum  eignesolver (VQE)** 

### 3. 