# Qiskit 

Qiskit is the fundamental thingy(best way to describe it at this point) that a lot of quantum computers will refer to nowadays (at least for the moment) from IBM. We  first have to create a conda environment (or a  xonsh one!) in which we install qiskiet and all necessary distributions. Here are the docs to get [started](https://qiskit.org/documentation/getting_started.html). 

---

There seems to be qiskit distributions for: 
- qiskit-visualization
- [qiskit-nature](obsidian://open?vault=Coding%20Tips&file=Qiskit-Nature)
- qiskiet-metal