#  LaTeX

LaTex is an extremely high quality document creator and type-setting software. You can produce sophisticated ``articles``, ``reports``,  ``books``, ``letters``, and ``slides`` this way for presentation  of information to the scientific community. 

Make sure to read the README.md file upon installation also found [here](https://www.tug.org/mactex/READ_ME_FIRST.pdf). 

You can learn more at[ learnlatex.org. ](https://www.learnlatex.org/en/lesson-05)

---


For a ``letter`` for instance, a document class will look like this: 

```
\documentclass{letter}
\usepackage[T1]{fontenc}
\begin{document}

\begin{letter}{Some Address\\Some Street\\Some City}

\opening{Dear Sir or Madam,}

This is an example of a highly sophisticated & educated correspondence.

\closing{Yours,}

\end{letter}

\end{document}
```


In another example, we may see an ``article`` document may be written: 

```
\documentclass{article} % Change the class here
\usepackage[T1]{fontenc}

\begin{document}

\section{Introduction}

This is a sample document with some dummy
text\footnote{and a footnote}. This paragraph is quite
long as we might want to see the effect of making the
document have two columns.

\end{document}
```

---

### LaTeX Presentations 

- some examples and templates can be found [here](atextemplates.com/template/beamer-presentation)
	- They are very clean and academic-looking 
- How to create your own LaTeX  Presentation! 
	- follow [this](https://texblog.org/2008/01/21/create-your-slides-presentations-with-latex/)  tutorial to get started  :)

We start by defining the document-class

```
\documentclass[landscape]{slides}
```

But you may be better off editing a document simply  through [Overleaf](https://www.overleaf.com/project/6306feb35a99fd317a79ec3a) as well.  

![[Pasted image 20220825004820.png]]