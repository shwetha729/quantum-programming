#  Arline Benchmarks 

The Arline Benchmarks are an independent  third-party analysis  tool to compare all the quantum processors that exist today  with no affiliation to any of them. This then removes any bias and only looks at processors  in terms of efficiency and speed-up. It has been used  for compiler performance testing and more. Furthermore, it autogenerates benchmarking reports in LaTeX so  you can get to experience that  as  well. You may also run it online [here](https://www.arline.io/arline-benchmarks) by uploading the qasm files and comparing. It currently works with Qiskit, Tket, PyZX, & VOQC. 

### Folder Structure 
The folder structure for  Arline Benchmarks once installed looks like this: 

```
arline_benchmarks
│
├── arline_benchmarks            # platform classes
│   ├── config_parser            # parser of pipeline configuration
│   ├── engine                   # pipeline engine
│   ├── metrics                  # metrics for pipeline comparison
|   ├── pipeline                 # pipeline
│   ├── reports                  # LaTeX report generator
│   ├── strategies               # list of strategies for mapping/compression/rebase
│   └── targets                  # target generator
│
├── circuits                     # qasm circuits dataset
│
├── configs                      # configuration files
│   └── compression              # config .jsonnet file and .sh scripts
│
├── docs                         # documentation
│
├── scripts                      # run files
│
└── test                         # tests
    ├── qasm_files               # .qasm files for test
    └── targets                  # test for targets module
```

