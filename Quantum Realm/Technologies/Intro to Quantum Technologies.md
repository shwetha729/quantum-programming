# Quantum Tech


Quantum technologies have the potential to dramatically change certain areas and industries as know it as we apply it and as the sector develops. There are 4 main kinds of quantum technologies that will come up: 

## 1. Quantum computers

-   These devices are here and san speed up certain calculations dramatically 
-   Not just both 0 and 1 but rather in between 0 and 1    
-   Can process much more information with qubits than bits
-   This speedup  only works with certain calculations 
-   To be useful  - need to bring  a large number of  qubits (~1 mill qubits)
-   Question is not will it work, it is rather **will it scale**    
	-  quantum computers already exist today
	-   yet status is similar to how nuclear fusion worked 50 years ago, effective and still in process 

## 2. Quantum internet 

-   Information transmitted with quantum effects
-   Uses quantum cryptography as a security protocol
-   It irreversibly changes the state of an information particle  
-   Cannot transfer info faster than speed of light or with any other quantum effect 
-   quantum computer can break current protocols 
-   Quantum internet can be safe from hacking by quantum computers
-   Caveat - Post quantum or quantum-safe cryptography
	-   People that work on quantum  things dont like to mention this  though

## 3. Quantum metrology 

-   Collection of measurements to improve quantum effects 
-   Medicine and material science 
-   Can make do with very few particles with minimal damage to sample
-   Most promising quantum technology

## 4. Quantum simulations 

-   Very useful in trying to understand complicated system 
-   By reproducing system that you can control better to better predict system
-   Dramatic shift in modern physics as you can take out mathematics 
-   Instead of simulating with mathematics you model it directly  with another system 
-   Simulate particles similar to the higgs which you cannot do in any other way 
	-   though headlines like the simulated wormhole is nonsense


via [source](https://www.youtube.com/watch?v=b-aGIvUomTA)