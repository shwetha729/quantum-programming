# Machine Learning  (QML)

Quantum machine learning  is one of the biggest pillars of quantum computing application that we will see in the workforce of the future 
One of the big contenders in this field is Xanadu.ai's [pennylane](https://pennylane.ai/) software. Recently version 0.25 was just released and a lot more applications can be created. 



The following questions can finally be answered with [this](https://pennylane.ai/blog/2022/08/pennylane-v025-released/#new-return-types-for-qnodes-with-multiple-measurements) release: 



#### How many qubits/gates do I need to run this algorithm? 

module for estimating through process of first and second quantization: 

```
# first quantization 

>>> n = 100000        # number of plane waves
>>> eta = 156         # number of electrons
>>> omega = 1145.166  # unit cell volume in atomic units
>>> algo = FirstQuantization(n, eta, omega)
>>> algo.gates
1.10e+13
>>> algo.qubits
4416
```
```
# second quantization with double factored Hamiltonian 
# Hamiltonia 

symbols  = ['O', 'H', 'H']
geometry = np.array([[0.00000000,  0.00000000,  0.28377432],
                    [0.00000000,  1.45278171, -1.00662237],
                    [0.00000000, -1.45278171, -1.00662237]], requires_grad = False)

mol = qml.qchem.Molecule(symbols, geometry, basis_name='sto-3g')
core, one, two = qml.qchem.electron_integrals(mol)()

algo = DoubleFactorization(one, two)
```
```

```

and voila! 

```

>>> print(algo.gates, algo.qubits)
103969925, 290
```



and even more capabilities are available the more you explore! :) 