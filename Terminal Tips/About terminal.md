# Your terminal is everything!

Your terminal is the main way that you interact with the machine. So you have to make sure that it is optimized to be as lightweight as possible. 

If you ever want to take a screen recording of the terminal just type in: 
```
asciinema rec
```

Copy and paste with [xclip](https://github.com/astrand/xclip)!  You can even [share](https://www.fosslife.org/2-tools-linux-terminal-sharing?cmid=4a5288c5-330b-4694-a774-0e44f9a5fa28) your terminal  screen with multiple users using screen & tmux. 

---


Eventually the goal is to port the info I've gather on [this](https://docs.google.com/document/d/1HSj4i64hM6291LXvIoWxMJfYn35geWJaAWY9oXkOmm0/edit#) document over to here..eventually.


- The whole bash versus zsh debate is nicely compared to on [this](https://stackabuse.com/zsh-vs-bash/) site. Both are great, use whichever you prefer. 

- There is a near infinite amount of things to play around  and improve on in your terminal - thus [Terminal To-do's](obsidian://open?vault=Coding%20Tips&file=Terminal%20To-do's) are born. 