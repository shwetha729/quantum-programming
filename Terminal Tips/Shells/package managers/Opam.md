# Opam 

Opam is an open-sourced package manager  for OCaml (a functional programming language  and system) and supports multiple simultaneous compiler installations, flexible package constraints, and a Git-friendly development workflow. This is a necessary  package install that is needed for many of the quantum programs that will be used. 

The full  install instructions  can  be found [here](https://github.com/ocaml/opam). 