# Fink 

Fink is a really useful package that allows you to use those **"sudo apt-get**" commands that you love so well rather than just relying on mac OS's "brew" commands. This is awesome because it opens up a whole new world of Linux packages that aren't previously as easily  available with the Apple command-tools. 

And you get to go back to using  your more  familiar Linux commands that you are more comfortable with anyways!  YAY! Time to work


---

#### quick how-to learned via [the internet](**[https://www.digimantra.com/howto/apple-aptget-command-mac/](https://www.digimantra.com/howto/apple-aptget-command-mac/)**) :') 
1.  Go to the [source](https://www.finkproject.org/download/srcdist.php) release page  and download appropriate package. Since I am mac 10.14 I am able to download the [helper script](https://github.com/fink/scripts/releases/tag/setup/0.45.6) package. (A note: this works only on macOS 10.9-10.15 currently) You should download all the three files you see on  the page. **![](https://lh6.googleusercontent.com/THW3OubFhoTD6gnwGVSr4dr7ewVvRmvZSbbyDP80YJJjlLMdbKdqZElReaeeuM2E6dptw84Yb8I2HSuQa0zF4l6mF7T1PMvyHuqCu1I0ytR-0OxGw5VN8zcZpZOsnhqY7Oqo3eqFfYaTE8MrLFtMsQE)**

2. You will make sure that you have xcode installed (or if you're  like me and never bothered to download xcode because it takes up WAYY too much storage space,  just  the command tools should suffice for ] 10.14) You can check for this by  ```sudo xcode-select --install```

3. When you double click on InstallFink (right click on "Open with" > "Terminal" if you're not using the safari browser like me) and it will open up in the terminal

4. Press any key to continue and press "n"  if you are not installing xcode (like previously stated, just the commant tools will suffice for 10.14)

5. Allow it to finish setting up and installing and voila!

6. Log off and log in and set up the environment via ```/sw/bin/pathsetup.sh```

7. You can press enter through a lot of the default configurations (side note  - your proxy:   http://shway:moon@jedisailor:00711 , FTP:  http://shway:moon@jedisailor:00719) 

8.  The basic commands to completing full set up  is:  
```
>>>cd fink-0.45.6

>>>./bootstrap (to  start bootstrap)  

>>>./bootstrap /path (where path is where your  directory will be)

>>>/sw/bin/pathsetup.sh (the default path if not specified)

```

9. Then open a new terminal and  in the /sw folder run the following:  
```
>>>fink selfupdate-rsync
>>>fink index -f

OR

>>>fink selfupdate-git
>>>fink index -f
```

10.   To use ```fink``` in terminals going forward  the setup.sh  script will automatically set up in your .profile
11.  Ta da! You can use fink install or sudo apt-get  now! For any further info, refer to the [docs](https://www.finkproject.org/doc/index.php). And also make sure to refer to [this](https://www.finkproject.org/doc/usage/index.php) for info on  fink keyword usage :)  Enjoy.


---


#### Things installed via Fink 
- **[Wikit](https://www.tecmint.com/wikipedia-commandline-tool/)** - package that allows you to wikipedia summary anything through terminal 
	- usage:  ```wikit <search-term> ```
- 