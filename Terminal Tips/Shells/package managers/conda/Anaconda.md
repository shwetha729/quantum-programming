# Anaconda python environment manager 

Anaconda is a great tool used by large machine learning projects and teams to create virtual environments to run their program on. It is often more useful than venv nowadays and a way to run your app without needing to containerize it through Docker or Kubernetes. 

#### Some important commands: 

To see all conda environments available: 
```
conda info --envs
```


For more detailed info, refer to the [docs](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html). 

---

**To return a list of all the virtual environments you've made, search:** 
```
find ~ -d -name "site-packages" 2>/dev/null
```