# Python Poetry 

A useful python project & package manager that exists within venv & converts to a readable directory. 

---


### Some important commands: 

- Install poetry using the installer: 
```
curl -sSL https://install.python-poetry.org | python3 -
```
(this is more [recommended](https://python.land/virtual-environments/python-poetry) than simply pip-installing)
this is currently installed in the  ```/Users/shwethajayaraj/.local/bin``` for me. 

Output: 
```
Installing Poetry (**1.1.14**): Done

  

Poetry (**1.1.14**) is installed now. Great!

  

To get started you need Poetry's bin directory (/Users/shwethajayaraj/.local/bin) in your `PATH`

environment variable.

  

Add `export PATH="/Users/shwethajayaraj/.local/bin:$PATH"` to your shell configuration file.

  

Alternatively, you can call Poetry explicitly with `**/Users/shwethajayaraj/.local/bin/poetry**`.

  

You can test that everything is set up by executing:

  

`**poetry --version**`
```

- Keeping poetry up to date: 
```
poetry self update
```


- Starting a project with Poetry 
```
poetry new demo 
```

- Install and remove packages
```
poetry add requests
```
```
poetry remove <package name>
```


- install dependences of existing python project (as listed in the pyproject.toml file)
```
poetry install
```

- convert existing project to poetry 
```
cd my-project

poetry init
```

- Starting a shell with the python virtual environment activated 
```
poetry shell
```


### More Useful commands: 
- Exporting requirement to a txt file
```
poetry export -f requirements.txt > requirements.txt
```
