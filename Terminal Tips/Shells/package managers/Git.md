# Git 

Git is a way to upload your packages onto a  version control system of your choice (as in either Gitlab, Github, or other version control site of your choice)

Here are some key commands to know: 


---

### CLI instructions: 

#### Git global setup: 

```
git config --global user.name "Shwetha Jayaraj"
git config --global user.email "shwetha.jayaraj@uconn.edu"
```

#### Create a new repository: 
```
git clone git@gitlab.com:shwetha729/coding-tips.git
cd coding-tips
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

```

-   When you have an empty folder to add to git repo 
```
Git init 
Git add . 
Git status 
Git commit -m ‘your message’ # commit to local repo
Git remote add origin ‘urlname’ # add url from repo 
Git push -u origin master # push local content to github
```


#### Push an existing folder: 
```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:shwetha729/coding-tips.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

Example 2: pushing changes from commit into your branch 
```
git push [nameofyournewremote] [url]
```

Example 3: 

#### Push an existing Git repository: 

Example 1: 
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:shwetha729/coding-tips.git
git push -u origin --all
git push -u origin --tags

```

Example 2: 
```
Git remote add origin https://github.com/username/repo.git
Git push -u origin master
```

Example 3: Forking an existing repo
```
#  fork the original repo  on top  right corners
git clone  [git url]
git remote -v # syncs forked repo 
git remote add upstream [git clone url]
```


Example 4:   Push changes from your commit into your branch 
```
git push [nameofyournewremote]  [url]
```


#### Branch controls 

- Create a new branch on git: 
```
$ git branch <name_of_your_new_branch>
```

- View all branches: 
```
git branch -a
```

- Add a new remote for your branch: 
```
git remote add [name_of_your_remote] [name_of_your_new_branch]
```

- Delete  a branch on your local filesystem: 
```
$ git branch -d [name_of_your_new_branch]

# or to force deletion 

$ git branch -D [name_of_your_new_branch]
```

-  Delete a branch  on github: 
```
$ git push origin :[name_of_your_new_branch]
```

- Go to desired branch 
```
git checkout [branchname]
# OR 
#git checkout --
```



---


#### SSH things:
- **CREATE  new ssh key:** 

For example, for ED25519:
```
ssh-keygen -t ed25519 -C "<comment>"
```

For 2048-bit RSA:

```
ssh-keygen -t rsa -b 2048 -C "<comment>"
```

and then
```
Generating public/private ed25519 key pair. Enter file in which to save the key (/home/user/.ssh/id_ed25519):
```
press enter twice. 

```
Enter passphrase (empty for no passphrase): Enter same passphrase again:
```
then enter passphrase. 

- **COPY public key to Gitlab account for example (MacOS):** 

```
tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy
```
Replace `id_ed25519.pub` with your filename. For example, use `id_rsa.pub` for RSA.

Then sign in > top right > Preferences > SSH Keys > paste contents into Key box > type description into Title box > Add Key .   

- **VERIFY that you  can connect** :

```
ssh -T git@gitlab.com

>>Welcome to GitLab, @shwetha729! 
```
For more info on SSH, check out [here](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair). 



- [Git for Obsidian](https://medium.com/analytics-vidhya/how-i-put-my-mind-under-version-control-24caea37b8a5) article 
- 



----

Other sources:

- a [git command cheat sheet ](https://dev.to/anitaparmar26/git-command-cheat-sheet-31ec)
- 


