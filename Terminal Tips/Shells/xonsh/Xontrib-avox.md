# Xontrib-avox 

This is a way to activate new environments and unactivate environments as you cd around projects. This is a lot more useful than conda as it treats projects as directories (similar to how obsidian works) .

Example: 

1. setting a project  as ```$PROJECT_DIRS = [p'~/code']``` 
2. setting a project directory as ```~/code/spam ``` 
3. ```avox``` then will use the venv name as ```spam```


### Installation 

```
pip install xontrib-avox
```

or just clone the original [repo](https://github.com/AstraLuma/xontrib-avox) with pip. 

#### Configuration 
You must configure  ```$PROJECT_DIRS```  by: 

```
$PROJECT_DIRS = ["~/code"]
```

