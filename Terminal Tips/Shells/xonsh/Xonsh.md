# Xonsh Stuff 

Xonsh is basically a terminal for python which is super cool but even better because you can literally also do bash commands inside it. 

Begin by simply typing  ``xonsh`` into the terminal.

## Setting up an environment 
There is entirely no need for conda here as it can be done with a simple vox command: 
1. Load the xontrig  by typing:  
```
xontrib load vox
```

2. Create a new environment: 
```
vox new world1
```

3. List any existing or activate the environment: 
```
$ vox list
Available environments:
    eggs
    world1
    spam
```

```
$ vox activate world1
 Activated "world1".
```

4. More info found here such as: [Exit or remove the environment ](https://xon.sh/python_virtual_environments.html)


---

- there is a xonsh [vim file ](https://xon.sh/python_virtual_environments.html) but to be quite honest, you already have kite installed and that seems a lot better. 
- if every in doubt just type in cheatsheet
- Make python extensions with [xontrib](https://asciinema.org/a/499605)


## Things installed with xontrib 
- xontrib-avox 
- xontrib loaded the [prompt-bar](https://github.com/anki-code/xontrib-prompt-bar)
- xontrib loaded the [cheatsheet](https://github.com/anki-code/xonsh-cheatsheet/blob/main/README.md
- 